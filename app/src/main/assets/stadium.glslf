#version 300 es

precision mediump float;
precision highp int;

uniform vec3 lightPos;
uniform int drawContour;
uniform int dissect;
uniform float fadeDegree;
in vec3 fragModel;
in vec3 transfNormal;
out vec4 fragColor;

void main() {
    if (drawContour == 1) {
        if (fragModel.z < fadeDegree)
            discard;

        fragColor=vec4(0,0,0,1);

        return;
    }

    vec3 lightDir = normalize(lightPos-fragModel);
    float diff = max(dot(lightDir, transfNormal),0.0);
    if (diff > 0.95) fragColor = vec4(0.85, 0.95, 0.25, 1);
    else if (diff > 0.5) fragColor = vec4(0.425, 0.474, 0.125, 1);
    else if (diff > 0.25) fragColor = vec4(0.2, 0.2, 0.075, 1);
    else fragColor = vec4(0.1, 0.1, 0,1);

    if (fragModel.z < fadeDegree)
        discard;
}