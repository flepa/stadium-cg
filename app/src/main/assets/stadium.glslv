#version 300 es

precision highp int;

layout(location = 1) in vec3 vPos;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec2 texCoord;
out vec2 varyingTexCoord;
uniform mat4 MVP;
uniform mat4 modelMatrix;
uniform mat4 inverseModel;
uniform int drawContour;
uniform float posIndex;
out vec3 fragModel;
out vec3 transfNormal;

void main() {
    vec2 newTexCoord = texCoord;
    newTexCoord.t += 0.2 * posIndex;
    varyingTexCoord = newTexCoord;
    float scaling = 1.1;
    if(drawContour==0) {
        transfNormal = normalize(vec3(inverseModel * vec4(normal, 1)));
        fragModel = vec3(modelMatrix * vec4(vPos, 1));
        scaling = 1.0;
    }
    gl_Position = MVP * vec4(vPos * scaling, 1);
    fragModel = vec3(modelMatrix * vec4(vPos, 1));
}