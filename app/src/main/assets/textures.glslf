#version 300 es

precision mediump float;

uniform sampler2D fieldTex;
in vec2 varyingTexCoord;
out vec4 fragColor;

void main() {
    vec4 tex;
    fragColor = texture(fieldTex, varyingTexCoord);
}