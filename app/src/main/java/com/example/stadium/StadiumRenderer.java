package com.example.stadium;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES10.GL_CCW;
import static android.opengl.GLES10.GL_TEXTURE0;
import static android.opengl.GLES20.GL_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_BACK;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_CULL_FACE;
import static android.opengl.GLES20.GL_DEPTH_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.GL_ELEMENT_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_LEQUAL;
import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_LINEAR_MIPMAP_NEAREST;
import static android.opengl.GLES20.GL_REPEAT;
import static android.opengl.GLES20.GL_STATIC_DRAW;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_UNSIGNED_INT;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindBuffer;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glBufferData;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glCullFace;
import static android.opengl.GLES20.glDepthFunc;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glFrontFace;
import static android.opengl.GLES20.glGenBuffers;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glGenerateMipmap;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glTexParameteri;
import static android.opengl.GLES20.glUniform1f;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glUniform3fv;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;

import com.example.computergraphics.utils.PlyObject;
import com.example.computergraphics.utils.ShaderCompiler;

public class StadiumRenderer extends BasicRenderer {
    private boolean dissect;  // flag variable which indicates if the FSM is in a final state or not
    // uniform locations:
    private int uInverseModel;
    private int uDrawContour;
    private int uDissect;
    private int uFadeDegree;
    private int texLocation;
    private int MVPLoc;
    private int MVPLocField;
    private int uModelM;
    private int posIndexLoc;
    // shader handles:
    private int shaderHandle;
    private int shaderHandleField;
    // variables used with GL calls
    private int countFacesToElementStadium;
    private int countFacesToElementField;
    private int[] VAO;
    private int[] texObjId;
    private final float[] viewM;
    private final float[] modelM;
    private final float[] projM;
    private final float[] MVP;
    private final float[] temp;
    private final float[] inverseModel;
    private final float[] lightPos;
    private final float[] eyePos;
    private final float[] lookAtPos;
    // variables for the application logic:
    private int angle;  // rotation angle
    private final float defaultAngle;
    private float rounds;  // how many 360° rounds have been done?
    private float fadeDegree;  // the parameter that is used to discard progressively the stadium
    private static String TAG;
    private final SportField sf;  // soccer field
    private final SportField bf;  // basketball field

    public StadiumRenderer() {
        super(0.8f, 0.84f, 0.68f, 1);

        dissect = false;
        defaultAngle = 30f;
        rounds = 0f;
        fadeDegree = -0.9f;
        TAG = getClass().getSimpleName();
        sf = new SportField("soccer");
        bf = new SportField("basketball");
        lightPos = new float[]{0, 0, 0};
        eyePos = new float[]{0f, 1f, -1.5f};
        lookAtPos = new float[]{0, 0, 0};
        viewM = new float[16];
        modelM = new float[16];
        projM = new float[16];
        MVP = new float[16];
        temp = new float[16];
        inverseModel = new float[16];
        Matrix.setIdentityM(inverseModel, 0);
        Matrix.setIdentityM(viewM, 0);
        Matrix.setIdentityM(modelM, 0);
        Matrix.setIdentityM(projM, 0);
        Matrix.setIdentityM(MVP, 0);
    }

    public SportField getSf() {
        return sf;
    }

    public SportField getBf() {
        return bf;
    }

    public boolean getDissect() {
        return dissect;
    }

    public void setDissect(boolean value) {
        dissect = value;
    }

    @Override
    @SuppressLint("ClickableViewAccessibility")
    public void setContextAndSurface(Context context, GLSurfaceView surface) {
        super.setContextAndSurface(context, surface);

        this.surface.setOnTouchListener(new View.OnTouchListener() {
            private final GestureDetector gestureDetector = new GestureDetector(
                    context,
                    new GestureDetector.SimpleOnGestureListener() {
                        @Override
                        public boolean onDoubleTap(MotionEvent event) {
                            setDissect(true);

                            if (!sf.isHidden()) {
                                sf.hideField();
                                bf.showField();
                            } else {
                                sf.showField();
                                bf.hideField();
                            }

                            return super.onDoubleTap(event);
                        }
                    }
            );

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                gestureDetector.onTouchEvent(motionEvent);

                return true;
            }
        });
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int w, int h) {
        super.onSurfaceChanged(gl10, w, h);
        float aspect = ((float) w) / ((float) (h == 0 ? 1 : h));

        Matrix.perspectiveM(projM, 0, 45f, aspect, 0.1f, 100f);
        Matrix.setLookAtM(viewM, 0, eyePos[0], eyePos[1], eyePos[2],
                lookAtPos[0], lookAtPos[1], lookAtPos[2],
                0, 1, 0);
    }

    private FloatBuffer allocateFloatBuffer(float[] array) {
        FloatBuffer floatBuffer =
                ByteBuffer.allocateDirect(array.length * Float.BYTES)
                        .order(ByteOrder.nativeOrder())
                        .asFloatBuffer();
        floatBuffer.put(array);
        floatBuffer.position(0);

        return floatBuffer;
    }

    private IntBuffer allocateIntBuffer(int[] array) {
        IntBuffer intBuffer =
                ByteBuffer.allocateDirect(array.length * Integer.BYTES)
                        .order(ByteOrder.nativeOrder())
                        .asIntBuffer();
        intBuffer.put(array);
        intBuffer.position(0);

        return intBuffer;
    }

    private void allocateGLBuffers(
            int[] VBO,
            FloatBuffer floatBuffer,
            IntBuffer intBuffer,
            int idxVAO,
            int stride,
            int[] offsets,
            int[] sizes,
            int[] locations
    ) {
        glGenBuffers(VBO.length, VBO, 0);

        GLES30.glBindVertexArray(VAO[idxVAO]);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
        glBufferData(
                GL_ARRAY_BUFFER,
                Float.BYTES * floatBuffer.capacity(),
                floatBuffer,
                GL_STATIC_DRAW
        );
        glVertexAttribPointer(  // vertex positions
                locations[0],
                sizes[0],
                GL_FLOAT,
                false,
                Float.BYTES * stride,
                offsets[0]
        );
        glVertexAttribPointer(  // texture coordinates or colors / normals
                locations[1],
                sizes[1],
                GL_FLOAT,
                false,
                Float.BYTES * stride,
                offsets[1] * Float.BYTES
        );
        glEnableVertexAttribArray(locations[0]);
        glEnableVertexAttribArray(locations[1]);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[1]);
        glBufferData(
                GL_ELEMENT_ARRAY_BUFFER,
                Integer.BYTES * intBuffer.capacity(),
                intBuffer,
                GL_STATIC_DRAW
        );

        GLES30.glBindVertexArray(0);
    }

    public void loadBitmap(int texObject, int resId, BitmapFactory.Options options) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resId, options);

        if (bitmap == null) {
            Log.v(TAG, "File not found!");
            System.exit(-1);
        }
        glBindTexture(GL_TEXTURE_2D, texObject);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        GLUtils.texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);
        glGenerateMipmap(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    private int createShader(String vertexSrc, String fragmentSrc) {
        InputStream isV;
        InputStream isF;
        int shader = 0;

        try {
            isV = context.getAssets().open(vertexSrc);
            isF = context.getAssets().open(fragmentSrc);

            shader = ShaderCompiler.createProgram(isV, isF);
            if (shader == -1)
                System.exit(-1);

            return shader;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return shader;
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        super.onSurfaceCreated(gl10, eglConfig);

        shaderHandle = createShader("stadium.glslv", "stadium.glslf");
        shaderHandleField = createShader("stadium.glslv", "textures.glslf");

        FloatBuffer vertexDataField = allocateFloatBuffer(sf.getSliceCoordinates());
        IntBuffer indexDataField = allocateIntBuffer(sf.getSliceIndices());
        countFacesToElementField = sf.getSliceIndices().length;

        VAO = new int[2];  // 0: fields, 1: stadium
        GLES30.glGenVertexArrays(2, VAO, 0);

        int[] VBO = new int[2];

        allocateGLBuffers(
                VBO,
                vertexDataField,
                indexDataField,
                0,
                5,
                new int[]{0, 3},  // offsets
                new int[]{3, 2},  // sizes
                new int[]{1, 3}   // variables' location
        );

        InputStream is;
        float[] verticesStadium = null;
        int[] indicesStadium = null;

        try {
            is = context.getAssets().open("stadium.ply");
            PlyObject po = new PlyObject(is);
            po.parse();
            verticesStadium = po.getVertices();
            indicesStadium = po.getIndices();
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }

        FloatBuffer vertexDataStadium = allocateFloatBuffer(verticesStadium);
        IntBuffer indexDataStadium = allocateIntBuffer(indicesStadium);
        countFacesToElementStadium = indicesStadium.length;

        VBO = new int[2];

        allocateGLBuffers(
                VBO,
                vertexDataStadium,
                indexDataStadium,
                1,
                6,
                new int[]{0, 3},  // offsets
                new int[]{3, 3},  // sizes
                new int[]{1, 2}   // variables' location
        );

        // stadium model
        MVPLoc = glGetUniformLocation(shaderHandle, "MVP");
        uModelM = glGetUniformLocation(shaderHandle, "modelMatrix");
        uInverseModel = glGetUniformLocation(shaderHandle, "inverseModel");
        int uLightPos = glGetUniformLocation(shaderHandle, "lightPos");
        uDrawContour = glGetUniformLocation(shaderHandle, "drawContour");
        uDissect = glGetUniformLocation(shaderHandle, "dissect");
        uFadeDegree = glGetUniformLocation(shaderHandle, "fadeDegree");

        // soccer field model
        MVPLocField = glGetUniformLocation(shaderHandleField, "MVP");
        posIndexLoc = glGetUniformLocation(shaderHandleField, "posIndex");
        texLocation = glGetUniformLocation(shaderHandleField, "fieldTex");

        // pre load uniform values:
        glUseProgram(shaderHandle);
        glUniform3fv(uLightPos, 1, lightPos, 0);
        glUseProgram(0);

        glUseProgram(shaderHandleField);
        glUniform1f(posIndexLoc, 0f);
        glUseProgram(0);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);

        texObjId = new int[2];
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;

        glGenTextures(texObjId.length, texObjId, 0);

        loadBitmap(texObjId[0], R.drawable.scfield, opts);
        loadBitmap(texObjId[1], R.drawable.bbfield, opts);
    }

    private void calculateAngle() {
        if (!dissect || angle % 360 != defaultAngle)
            angle += 1;
    }

    public void calculateFadeDegree() {
        if (dissect && fadeDegree <= -0.1f) {
            fadeDegree += 0.015f;
        } else if (!dissect && fadeDegree >= -0.9f) {
            fadeDegree -= 0.015f;
        }
    }

    private void drawStadium() {
        Matrix.multiplyMM(temp, 0, projM, 0, viewM, 0);

        Matrix.setIdentityM(modelM, 0);
        Matrix.translateM(modelM, 0, 0, 0, 0);
        Matrix.rotateM(modelM, 0, (float) angle, 0, 1f, 0);

        // calculate and send MVP
        Matrix.multiplyMM(MVP, 0, temp, 0, modelM, 0);
        glUniformMatrix4fv(MVPLoc, 1, false, MVP, 0);

        glUniform1i(uDissect, dissect ? 1 : 0);

        glUniform1f(uFadeDegree, fadeDegree);

        // draw contours
        glDisable(GL_DEPTH_TEST);
        glUniform1i(uDrawContour, 1);
        glDrawElements(GL_TRIANGLES, countFacesToElementStadium, GL_UNSIGNED_INT, 0);
        glUniform1i(uDrawContour, 0);
        glEnable(GL_DEPTH_TEST);

        // draw regular model
        glUniformMatrix4fv(uModelM, 1, false, modelM, 0);
        Matrix.invertM(inverseModel, 0, modelM, 0);
        glUniformMatrix4fv(uInverseModel, 1, true, inverseModel, 0);

        glDrawElements(GL_TRIANGLES, countFacesToElementStadium, GL_UNSIGNED_INT, 0);
    }

    private void activateFieldsTextures() {
        for (int i = 0; i < texObjId.length; i++) {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, texObjId[i]);
        }
    }

    private void drawField(int texUnit, SportField field) {
        glUniform1i(texLocation, texUnit);

        for (int i = 0; i < field.getLenSlices(); i++) {
            Matrix.setIdentityM(modelM, 0);
            Matrix.translateM(modelM, 0, 0, 0, 0);
            Matrix.rotateM(modelM, 0, angle, 0, 1f, 0);
            Matrix.translateM(
                    modelM, 0, field.getXPosByIndex(i), field.getYPosByIndex(i), 0
            );
            Matrix.multiplyMM(MVP, 0, temp, 0, modelM, 0);
            glUniformMatrix4fv(MVPLocField, 1, false, MVP, 0);
            glUniform1f(posIndexLoc, i);
            glDrawElements(GL_TRIANGLES, countFacesToElementField, GL_UNSIGNED_INT, 0);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // drawing the stadium
        calculateAngle();

        glUseProgram(shaderHandle);
        GLES30.glBindVertexArray(VAO[1]);

        drawStadium();

        // drawing the fields
        activateFieldsTextures();

        glUseProgram(shaderHandleField);
        GLES30.glBindVertexArray(VAO[0]);

        drawField(0, sf);
        drawField(1, bf);
    }

    public void reachFinalStatus(SportField hiddenField, SportField visibleField) {
        setDissect(false);
        hiddenField.setHidden(true);
        visibleField.setHidden(false);
    }
}