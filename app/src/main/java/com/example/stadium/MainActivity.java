package com.example.stadium;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import java.util.Objects;
import java.util.Timer;

public class MainActivity extends AppCompatActivity {
    private GLSurfaceView surface;
    private boolean isSurfaceCreated;
    private FrameMovementTask task;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getSupportActionBar()).hide();
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // get a reference to the Activity Manager (AM)
        final ActivityManager activityManager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        // from the AM we get an object with our mobile device info
        final ConfigurationInfo configurationInfo =
                activityManager.getDeviceConfigurationInfo();

        int supported = 1;
        if (configurationInfo.reqGlEsVersion >= 0x30000)
            supported = 3;
        else if (configurationInfo.reqGlEsVersion >= 0x20000)
            supported = 2;

        Log.v("TAG", "Opengl ES supported >= " +
                supported + " (" + Integer.toHexString(configurationInfo.reqGlEsVersion) + " " +
                configurationInfo.getGlEsVersion() + ")");

        surface = new GLSurfaceView(this);
        surface.setEGLContextClientVersion(supported);
        surface.setPreserveEGLContextOnPause(true);

        StadiumRenderer renderer = new StadiumRenderer();
        timer = new Timer();
        task = new FrameMovementTask(renderer);
        timer.schedule(task, 500, 40);

        setContentView(surface);
        renderer.setContextAndSurface(this, surface);
        surface.setRenderer(renderer);
        isSurfaceCreated = true;
    }

    @Override
    public void onStart() {
        super.onStart();

        task.wakeUp();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isSurfaceCreated)
            surface.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (isSurfaceCreated)
            surface.onPause();

        task.sleep();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        task.cancel();
        timer.purge();
        timer.cancel();
    }
}