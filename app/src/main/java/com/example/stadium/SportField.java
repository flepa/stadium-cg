package com.example.stadium;

/*
 mapping vertices to s,t texture coordinates
        float[] verticesScField = new float[]{
                -0.3f, -0.22f, -0.6f, 0f, 0f,
                0.3f, -0.22f, 0.4f, 1f, 1f,
                0.3f, -0.22f, -0.6f, 0f, 1f,
                -0.3f, -0.22f, 0.4f, 1f, 0f
        };
*/

public class SportField {
    private boolean hidden;  // status variable -> is the field hidden or not?
    private boolean isHiding;  // status variable -> is the field hiding or not?
    private final int lenSlices;   // number of rectangles that compose a field
    private final int[] sliceIndices;
    private final float translateParam;  // the "speed" that each slice has while translating
    private final float hiddenCoordX;  // the x value that the slices have while they are hidden
    private final float visibleCoordX;  // the x value that the slices have while they are visible
    private final float stepX;  // the geometrical length of each slice on the X-axis
    private final float hiddenCoordY;  // the y value that the slices have while they are hidden
    private final float visibleCoordY;  // the y value that the slices have while they are visible
    private final float stepY;  // the geometrical gap between each slice on the Y-axis
    private final float epsilon = (float) 1e-6;
    private final float[] sliceCoordinates;
    private final float[] sliceCurX;  // the current x coordinate of each slice, used during translation
    private final float[] sliceX;  // the shared data structure which contains the x position of each slice
    private final float[] sliceCurY;  // the current y coordinate of each slice, used during translation
    private final float[] sliceY;  // the shared data structure which contains the y position of each slice
    private final String fieldType;  // it can be soccer or basketball, used to configure different parameters

    public SportField(String fieldType) {
        this.fieldType = fieldType;
        hidden = !fieldType.equals("soccer");
        isHiding = false;
        translateParam = 0.01f;
        hiddenCoordX = fieldType.equals("soccer") ? 0.3f: -0.3f;
        visibleCoordX = -0.23f;
        stepX = 0.12f;
        hiddenCoordY = fieldType.equals("soccer") ? -0.1f : -0.5f;
        visibleCoordY = 0f;
        stepY = 0.1f;
        lenSlices = 5;
        sliceCurX = new float[lenSlices];
        sliceX = new float[lenSlices];
        sliceCurY = new float[lenSlices];
        sliceY = new float[lenSlices];
        sliceCoordinates = new float[]{
                -0.06f, -0.23f, -0.4f, 0f, 0f,
                0.06f, -0.23f, 0.5f, 1f, 0.2f,
                0.06f, -0.23f, -0.4f, 0f, 0.2f,
                -0.06f, -0.23f, 0.5f, 1f, 0f
        };
        sliceIndices = new int[]{
                0, 1, 2,
                0, 3, 1
        };
    }

    public float[] getSliceCoordinates() {
        return sliceCoordinates;
    }

    public int[] getSliceIndices() {
        return sliceIndices;
    }

    public int getLenSlices() {
        return lenSlices;
    }

    public void saveCurCoordinates() {
        for (int i = 0; i < lenSlices; i++) {
            sliceCurX[i] = getDefaultXPosByIndex(i);
            sliceCurY[i] = getDefaultYPosByIndex(i);
        }
    }

    public void hideField() {
        isHiding = true;
        saveCurCoordinates();
    }

    public void showField() {
        isHiding = false;
        saveCurCoordinates();
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean value) {
        hidden = value;
    }

    private int getLastSliceIdx() {
        return fieldType.equals("soccer") ? 0 : lenSlices - 1;
    }

    public boolean isCompletelyHidden() {
        int lastSliceIdx;
        int signY = fieldType.equals("soccer") ? -1 : 1;
        float fixedX = hiddenCoordX;
        float fixedY;

        lastSliceIdx = getLastSliceIdx();
        fixedY = hiddenCoordY + signY * stepY * lastSliceIdx;

        return (sliceX[lastSliceIdx] <= fixedX + epsilon)
                && (sliceX[lastSliceIdx] >= fixedX - epsilon)
                && (sliceY[lastSliceIdx] <= fixedY + epsilon);
    }

    public boolean isCompletelyVisible() {
        float fixedX;
        float fixedY = visibleCoordY;
        int lastSliceIdx;

        lastSliceIdx = getLastSliceIdx();
        fixedX = visibleCoordX + stepX * lastSliceIdx;

        return (sliceX[lastSliceIdx] <= fixedX + epsilon)
                && (sliceX[lastSliceIdx] >= fixedX - epsilon)
                && (sliceY[lastSliceIdx] <= fixedY + epsilon)
                && (sliceY[lastSliceIdx] >= fixedY - epsilon);
    }

    public void setXPosByIndex(int index, boolean translating) {
        if (translating)
            sliceX[index] = getTransientXPosByIndex(index);
        else
            sliceX[index] = getDefaultXPosByIndex(index);
    }

    public float getXPosByIndex(int index) {
        return sliceX[index];
    }

    public float getDefaultXPosByIndex(int index) {
        float position = visibleCoordX + stepX * index;

        if (hidden)
            position = hiddenCoordX;

        return position;
    }

    public float getHidingCoordXByIndex(int index) {
        if (fieldType.equals("soccer")) {
            if (sliceCurX[index] < hiddenCoordX - epsilon)
                sliceCurX[index] += translateParam;
        } else {
            if (sliceCurX[index] > hiddenCoordX + epsilon)
                sliceCurX[index] -= translateParam;
        }
        return sliceCurX[index];
    }

    public float getShowingCoordXByIndex(int index) {
        float fixedX = visibleCoordX + stepX * index;
        float fixedY = visibleCoordY;

        if (fieldType.equals("soccer")) {
            if ((sliceCurY[index] <= fixedY + epsilon)
                    && (sliceCurY[index] >= fixedY - epsilon)
                    && (sliceCurX[index] > fixedX - epsilon))
                sliceCurX[index] -= translateParam;
        } else {
            if ((sliceCurY[index] <= fixedY + epsilon)
                    && (sliceCurY[index] >= fixedY - epsilon)
                    && (sliceCurX[index] < fixedX - epsilon))
                sliceCurX[index] += translateParam;
        }
        return sliceCurX[index];
    }

    public float getTransientXPosByIndex(int index) {
        if (isHiding)
            return getHidingCoordXByIndex(index);

        return getShowingCoordXByIndex(index);
    }

    public void setYPosByIndex(int index, boolean translating) {
        if (translating)
            sliceY[index] = getTransientYPosByIndex(index);
        else
            sliceY[index] = getDefaultYPosByIndex(index);
    }

    public float getYPosByIndex(int index) {
        return sliceY[index];
    }

    public float getDefaultYPosByIndex(int index) {
        float position = visibleCoordY;
        int signY = fieldType.equals("soccer") ? -1 : 1;

        if (hidden)
            position = hiddenCoordY + signY * stepY * index;

        return position;
    }

    public float getHidingCoordYByIndex(int index) {
        int signY = fieldType.equals("soccer") ? -1 : 1;
        float fixedX = hiddenCoordX;
        float fixedY = hiddenCoordY + signY * stepY * index;

        if ((sliceCurX[index] <= fixedX + epsilon)
                && (sliceCurX[index] >= fixedX - epsilon)
                && sliceCurY[index] > fixedY)
            sliceCurY[index] -= translateParam;

        return sliceCurY[index];
    }

    public float getShowingCoordYByIndex(int index) {
        if (sliceCurY[index] < visibleCoordY - epsilon)
            sliceCurY[index] += translateParam;

        return sliceCurY[index];
    }

    public float getTransientYPosByIndex(int index) {
        if (isHiding)
            return getHidingCoordYByIndex(index);

        return getShowingCoordYByIndex(index);
    }
}
