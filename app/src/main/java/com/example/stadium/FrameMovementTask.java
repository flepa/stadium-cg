package com.example.stadium;

import java.util.TimerTask;

public class FrameMovementTask extends TimerTask {
    private final StadiumRenderer renderer;
    private boolean awake;
    private final Object lock;

    public FrameMovementTask(StadiumRenderer renderer) {
        this.renderer = renderer;
        awake = true;
        lock = new Object();
    }

    public void updateFieldPosition(SportField field, boolean translating) {
        for (int i = 0; i < field.getLenSlices(); i++) {
            field.setXPosByIndex(i, translating);
            field.setYPosByIndex(i, translating);
        }
    }

    public void translateFields(SportField curHiddenField, SportField curVisibleField) {
        updateFieldPosition(curVisibleField, true);
        updateFieldPosition(curHiddenField, curVisibleField.isCompletelyHidden());
        if (curHiddenField.isCompletelyVisible())
            renderer.reachFinalStatus(curVisibleField, curHiddenField);
    }

    @Override
    public void run() {
        if (!awake) {
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException ignored) {}
            }
        }

        renderer.calculateFadeDegree();

        if (!renderer.getDissect()) {
            updateFieldPosition(renderer.getSf(), false);
            updateFieldPosition(renderer.getBf(), false);
            return;
        }

        if (renderer.getSf().isHidden())  // wait until the basketball field is completely hidden
            translateFields(renderer.getSf(), renderer.getBf());
        else  // wait until the soccer field is completely hidden
            translateFields(renderer.getBf(), renderer.getSf());
    }

    public void wakeUp() {
        awake = true;
        synchronized (lock) {
            lock.notify();
        }
    }

    public void sleep() {
        awake = false;
    }
}
