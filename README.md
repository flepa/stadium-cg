# :stadium: Stadium concept

**Stadium** is a Computer Graphics project developed as an Android app with OpenGLES. It's just a **really** basic application that just shows a rotating stadium with a soccer field and, if the user taps two times the screen, the stadium starts dissecting and shows a *field exchange* between two fields (soccer and basketball). After that, the other field is showed and the user can repeat the same operations as before in a infinite loop. 

You can see a demo in this [video](https://drive.google.com/file/d/19GF8a_T9-JbGArmG79uIq_kfCKElojDp/view?usp=sharing). 

## Overview

The code base is written in **Java** and the main Android Development Guidelines are respected.  
In order to make the animations smooth, a TimerTask is used to change the coordinates values periodically.

You can think the application (and the stadium) as a finite state machine (FSM). The logic is the following:  

[![](https://mermaid.ink/img/pako:eNp1kMGKwjAQhl9lmFMLFddrKQW17Gn34u4xl2imZjBtSpK6LOq7Gw1VEMxpCB_fP_OfcGcVYYl7JwcNXxvRQ3zL7JPJKPiAI3veGsphNqvPjR3jDL9yOMMqEzhBmhV54ODJtJA53uuQQ7V18xoq7WqYOK_t35Mz1IZcYA4pc3WLgPVDu3jB32kXL_GTNlnXd2uTTezbexKYllhigR25TrKK1ZxuJoFBU0cCyzgq6Q4CRX-JnByD_fnvd1gGN1KB46BkoIZlbLTDspXGx19SHKz7Tl3fK79cAUCtdYQ?type=png)](https://mermaid.live/edit#pako:eNp1kMGKwjAQhl9lmFMLFddrKQW17Gn34u4xl2imZjBtSpK6LOq7Gw1VEMxpCB_fP_OfcGcVYYl7JwcNXxvRQ3zL7JPJKPiAI3veGsphNqvPjR3jDL9yOMMqEzhBmhV54ODJtJA53uuQQ7V18xoq7WqYOK_t35Mz1IZcYA4pc3WLgPVDu3jB32kXL_GTNlnXd2uTTezbexKYllhigR25TrKK1ZxuJoFBU0cCyzgq6Q4CRX-JnByD_fnvd1gGN1KB46BkoIZlbLTDspXGx19SHKz7Tl3fK79cAUCtdYQ)
